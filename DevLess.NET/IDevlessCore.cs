﻿using DevLess.NET.Response;
using DevLess.NET.Response.Core;
using DevLess.NET.Response.Service;
using System.Threading.Tasks;

namespace DevLess.NET
{
    public interface IDevlessCore
    {
        Task<AuthResponse<SignupResult>> SignUp(string username, string email, string password, string firstName, string lastName, string phoneNumber, string role = null);

        Task<AuthResponse<LoginResult>> Login(string password, string username = null, string email = null, string phoneNumber = null);
    }
}