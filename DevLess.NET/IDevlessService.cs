﻿using DevLess.NET.Request.Service;
using DevLess.NET.Response.Service;
using System;
using System.Threading.Tasks;

namespace DevLess.NET
{
    public interface IDevlessService
    {
        Task<ReadResponse<T>> Read<T>(string tableName, Query query);

        Task<CreateResponse> Create(string tableName, object resource);

        Task Update(string tableName, long id, object record);

        Task Update(string tableName, Tuple<string, object> where, object record);

        Task Delete(string tableName, long id);

        Task Delete(string tableName, Tuple<string, object> where);

        Task Truncate(string tableName);


        Task CreateTable(TableDefinition tableDefinition);
    }
}