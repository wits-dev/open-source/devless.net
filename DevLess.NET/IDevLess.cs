﻿using DevLess.NET.Request;
using DevLess.NET.Response;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DevLess.NET
{
    public interface IDevLess
    {
        IDevlessCore Core();

        IDevlessService Service(string serviceName);

        Task<T> RpcCall<T>(string service, string action, params string[] parameters);

        Task<T> ServiceCall<T>(string service, string resource, object request);
    }
}
