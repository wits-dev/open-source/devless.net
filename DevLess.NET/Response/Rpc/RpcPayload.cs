﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevLess.NET.Response.Rpc
{
    internal class RpcPayload<T>
    {
        public int Id { get; set; }
        public int JsonRPC { get; set; }
        public T Result { get; set; }
    }
}
