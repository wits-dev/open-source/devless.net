﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevLess.NET.Response
{
    internal class ResponseBody<T>
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public T Payload { get; set; }
    }
}
