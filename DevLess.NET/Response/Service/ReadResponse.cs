﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevLess.NET.Response.Service
{
    public class ReadResponse<T>
    {
        public T[] Results { get; set; }
        public ReadResponseProperties Properties { get; set; }
    }
}
