﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevLess.NET.Response.Service
{
    public class ReadResponseProperties
    {
        public long Count { get; set; }
        public long CurrentCount { get; set; }
    }
}
