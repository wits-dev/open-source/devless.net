﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevLess.NET.Response.Core
{
    public class AuthResponse<T>
    {
        public T Profile { get; set; }
        public string Token { get; set; }
    }
}
