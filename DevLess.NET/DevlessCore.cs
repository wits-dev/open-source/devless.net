﻿using DevLess.NET.Response;
using DevLess.NET.Response.Core;
using DevLess.NET.Response.Service;
using System.Threading.Tasks;

namespace DevLess.NET
{
    public class DevlessCore : IDevlessCore
    {
        private readonly IDevLess _devLess;

        public DevlessCore(IDevLess devLess)
        {
            _devLess = devLess;
        }

        public async Task<AuthResponse<LoginResult>> Login(string password, string username = null, string email = null, string phoneNumber = null)
        {
            return await _devLess.RpcCall<AuthResponse<LoginResult>>("devless", "login", username, email, phoneNumber, password).ConfigureAwait(false);
        }

        public async Task<AuthResponse<SignupResult>> SignUp(string username, string email, string password, string firstName, string lastName, string phoneNumber, string role = null)
        {
            return await _devLess.RpcCall<AuthResponse<SignupResult>>(email, password, username, phoneNumber, firstName, lastName, role).ConfigureAwait(false);
        }
    }
}