﻿using DevLess.NET.Request;
using DevLess.NET.Request.Rpc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DevLess.NET
{
    public class DevLess : IDevLess
    {
        private readonly string _hostUrl;
        private readonly string _apiToken;
        private readonly string _apiVersion;

        public DevLess(string hostUrl, string apiToken, string apiVersion = "v1")
        {
            _hostUrl = hostUrl;
            _apiToken = apiToken;
            _apiVersion = apiVersion;
        }

        public IDevlessCore Core()
        {
            return new DevlessCore(this);
        }

        public IDevlessService Service(string serviceName)
        {
            return new DevlessService(serviceName, this);
        }

        public Task<T> RpcCall<T>(string service, string action, params string[] parameters)
        {
            var request = new RpcRequest()
            {
                Method = service,
                Params = parameters,
            };
            var endpoint = $"service/{service}/rpc?action={action}";


            throw new NotImplementedException();
        }

        public Task<T> ServiceCall<T>(string service, string resource, object request)
        {
            var serviceRequest = new ServiceRequest()
            {
                Resource = new object[] { request }
            };
            var endpoint = $"service/{service}/{resource}";

            throw new NotImplementedException();
        }
    }
}
