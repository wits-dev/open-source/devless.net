﻿using DevLess.NET.Request.Service;
using DevLess.NET.Response.Service;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WITS.Http;

namespace DevLess.NET
{
    public class DevlessService : IDevlessService
    {
        private readonly string _serviceName;
        private readonly IDevLess _devLess;

        public DevlessService(string serviceName, IDevLess devLess)
        {
            _serviceName = serviceName;
            _devLess = devLess;
        }

        public async Task<CreateResponse> Create(string tableName, object record)
        {

            var resource = new
            {
                name = tableName,
                field = RecordToDictionary(record),

            };

            return await DatabaseCall<CreateResponse>(resource).ConfigureAwait(false);
        }

        public async Task<ReadResponse<T>> Read<T>(string tableName, Query query)
        {
            return await DatabaseCall<ReadResponse<T>>(query.ToQueryString()).ConfigureAwait(false);
        }

        public async Task Update(string tableName, long id, object record)
        {
            await Update(tableName, Tuple.Create<string, object>("id", id), record).ConfigureAwait(false);
        }

        public async Task Update(string tableName, Tuple<string, object> where, object record)
        {
            (var field, var value) = where;

            var resource = new
            {
                name = tableName,
                @params = new[] {
                    new
                    {
                        where = $"{field},{Convert.ToString(value)}",
                        data = RecordToDictionary(record),
                    }
                }
            };

            await DatabaseCall<object>(resource).ConfigureAwait(false);
        }

        public async Task Delete(string tableName, long id)
        {
            await Delete(tableName, Tuple.Create<string, object>("id", id)).ConfigureAwait(false);
        }

        public async Task Delete(string tableName, Tuple<string, object> where)
        {
            (var field, var value) = where;

            var resource = new
            {
                name = tableName,
                @params = new[] {
                    new
                    {
                        where = $"{field},{Convert.ToString(value)}",
                        delete = true,
                    }
                }
            };

            await DatabaseCall<object>(resource).ConfigureAwait(false);
        }

        public async Task Truncate(string tableName)
        {
            var resource = new
            {
                name = tableName,
                @params = new[] {
                    new
                    {
                        truncate = true,
                    }
                }
            };

            await DatabaseCall<object>(resource).ConfigureAwait(false);
        }

        public async Task CreateTable(TableDefinition tableDefinition)
        {
            await _devLess.ServiceCall<object>(_serviceName, "schema", tableDefinition).ConfigureAwait(false);
        }


        protected async Task<T> DatabaseCall<T>(object request)
        {
            return await _devLess.ServiceCall<T>(_serviceName, "db", request).ConfigureAwait(false);
        }

        protected IDictionary RecordToDictionary(object record)
        {
            var dict = record as IDictionary;
            if (dict is null)
            {
                dict = (
                    from x in record.GetType().GetProperties() select x).ToDictionary(
                        x => x.Name,
                        x => x.GetGetMethod().Invoke(record, null)
                    );
            }

            return dict;
        }
    }
}