﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DevLess.NET.Request.Service
{
    public class Query
    {
        private Dictionary<string, string> args = new Dictionary<string, string>();

        private List<Tuple<string, string>> query = new List<Tuple<string, string>>();


        public Query WhereEqual(string columnName, object value)
        {
            WhereInternal("where", columnName, value);
            return this;
        }

        public Query OrWhereEqual(string columnName, object value)
        {
            WhereInternal("orWhere", columnName, value);
            return this;
        }

        public Query WhereLike(string columnName, object value)
        {
            WhereInternal("search", columnName, value);
            return this;
        }

        public Query WhereGreaterThan(string columnName, decimal value)
        {
            WhereInternal("greaterThan", columnName, value);
            return this;
        }

        public Query WhereGreaterThanOrEqual(string columnName, decimal value)
        {
            WhereInternal("greaterThanEqual", columnName, value);
            return this;
        }

        public Query WhereLessThan(string columnName, decimal value)
        {
            WhereInternal("lessThan", columnName, value);
            return this;
        }

        public Query WhereLessThanOrEqual(string columnName, decimal value)
        {
            WhereInternal("lessThanEqual", columnName, value);
            return this;
        }


        public Query OrderBy(string columnName)
        {
            args.Add("orderBy", columnName);
            return this;
        }


        public Query Offset(long offset)
        {
            args.Add("offset", Convert.ToString(offset));
            return this;
        }

        public Query Limit(long limit)
        {
            args.Add("size", Convert.ToString(limit));
            return this;
        }


        internal string ToQueryString()
        {
            return string.Join("&", 
                    string.Join("&", query.Select((k, v) => $"{k}={v}")),
                    string.Join("&", args.Select((k, v) => $"{k}={v}"))
            );
        }

        private void WhereInternal(string op, string columnName, object value)
        {
            query.Add(Tuple.Create(op, $"{columnName},{Convert.ToString(value)}"));
        }
    }
}