﻿using Newtonsoft.Json;
using System;

namespace DevLess.NET.Request.Service
{
    public class TableFieldDefinition
    {
        public string Name { get; private set; }

        [JsonProperty("field_type")]
        public string Type { get; private set; }

        public bool Required { get; private set; }

        public string Default { get; private set; }

        [JsonProperty("is_unique")]
        public bool IsUnique { get; private set; }

        [JsonProperty("ref_table")]
        public string RefTable { get; private set; }

        public bool Validation { get; private set; }

        private TableFieldDefinition() { }

        public class Builder
        {
            public TableFieldDefinition definition = new TableFieldDefinition();

            public Builder Name(string name)
            {
                definition.Name = name;
                return this;
            }

            public Builder Type(TableFieldType type)
            {
                definition.Type = Convert.ToString(type).ToLower();
                return this;
            }

            public Builder Required(bool required)
            {
                definition.Required = required;
                return this;
            }

            public Builder Default(bool default_)
            {
                definition.Default = Convert.ToString(default_);
                return this;
            }

            public Builder IsUnique(bool isUnique)
            {
                definition.IsUnique = isUnique;
                return this;
            }

            public Builder RefTable(string refTable)
            {
                definition.RefTable = refTable;
                return this;
            }

            public Builder Validation(bool validation)
            {
                definition.Validation = validation;
                return this;
            }

            public TableFieldDefinition Build()
            {
                return definition;
            }
        }
    }
}