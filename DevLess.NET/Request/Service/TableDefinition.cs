﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DevLess.NET.Request.Service
{
    public class TableDefinition
    {
        public string Name { get; private set; }

        public string Description { get; private set; }

        [JsonProperty("field")]
        public List<TableFieldDefinition> Fields { get; } = new List<TableFieldDefinition>();

        private TableDefinition() { }

        public class Builder
        {
            public TableDefinition definition = new TableDefinition();

            public Builder Name(string name)
            {
                definition.Name = name;
                return this;
            }

            public Builder Description(string description)
            {
                definition.Description = description;
                return this;
            }

            public Builder Field(Func<TableFieldDefinition.Builder, TableFieldDefinition.Builder> cfgFunc)
            {
                definition.Fields.Add(cfgFunc(new TableFieldDefinition.Builder()).Build());
                return this;
            }

            public TableDefinition Build()
            {
                return definition;
            }
        }
    }
}