﻿namespace DevLess.NET.Request.Service
{
    public enum TableFieldType
    {
        Text,
        TextArea,
        Integer, 
        Decimal,
        Password,
        Percentage,
        Url,
        Timestamp,
        Boolean,
        Email,
        Reference
    }
}