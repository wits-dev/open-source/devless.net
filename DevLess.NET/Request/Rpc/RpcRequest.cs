﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevLess.NET.Request.Rpc
{
    internal class RpcRequest
    {
        public string Id { get; } = "1000";

        [JsonProperty("jsonrpc")]
        public string JsonRPC { get; } = "2.0";

        public string Method { get; set; }

        public string[] Params { get; set; } = new string[0];
    }
}